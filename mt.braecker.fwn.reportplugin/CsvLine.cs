﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mt.braecker.fwn.reportplugin
{
    public class CsvLine
    {
        public List<Item> Items { get; set; } = new List<Item>();
    }
}
