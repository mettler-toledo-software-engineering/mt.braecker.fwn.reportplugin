﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Spire.Pdf;
using Spire.Pdf.Exporting.XPS.Schema;

namespace mt.braecker.fwn.reportplugin
{
    public class PdfPrinterService
    {
        private readonly string _pdfpath;
        private readonly string _pdfname;
        private readonly string _printerName;

        public PdfPrinterService(PluginConfiguration configuration)
        {
            _pdfpath = configuration.WorkFolderPath;
            _pdfname = configuration.PdfFile;
            _printerName = configuration.PrinterName;
        }

        public void Print()
        {
            var pdf = System.IO.Path.Combine(_pdfpath,_pdfname);
            //PrinterSettings printersettings = new PrinterSettings();
            //printersettings.PrinterName = _printerName;

            //PrintDocument printDoc = new PrintDocument();

            //printDoc.DocumentName = pdf;
            //printDoc.PrinterSettings = printersettings;

            //printDoc.Print();

            PdfDocument pdfDocument = new PdfDocument();
            pdfDocument.LoadFromFile(pdf);
            pdfDocument.PrintSettings.PrinterName = _printerName;
            pdfDocument.Print();


        }
    }
}
