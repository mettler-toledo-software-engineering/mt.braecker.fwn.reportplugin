﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mt.braecker.fwn.reportplugin
{
    public class CheckForCsvService
    {
        private readonly PluginConfiguration _configuration;

        public CheckForCsvService(PluginConfiguration configuration)
        {
            _configuration = configuration;
        }

        public bool FileFound()
        {
            string csvSourceFile = Path.Combine(_configuration.ImportFolderPath, _configuration.ImportFileName);
            string csvDestinationFile = Path.Combine(_configuration.WorkFolderPath, _configuration.ImportFileName);

            if (File.Exists(csvDestinationFile))
            {
                return false;
            }

            if (File.Exists(csvSourceFile))
            {
                File.Move(csvSourceFile, csvDestinationFile, false);
                return true;
            }

            return false;
        }
    }
}
