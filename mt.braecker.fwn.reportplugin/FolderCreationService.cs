﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mt.braecker.fwn.reportplugin
{
    public class FolderCreationService
    {
        private readonly PluginConfiguration _configuration;

        public FolderCreationService(PluginConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void CreateNecessaryFolders()
        {
            if (!Directory.Exists(_configuration.DoneFolderPath))
            {
                Directory.CreateDirectory(_configuration.DoneFolderPath);
            }
            if (!Directory.Exists(_configuration.ImportFolderPath))
            {
                Directory.CreateDirectory(_configuration.ImportFolderPath);
            }
            if (!Directory.Exists(_configuration.WorkFolderPath))
            {
                Directory.CreateDirectory(_configuration.WorkFolderPath);
            }

        }
    }
}
