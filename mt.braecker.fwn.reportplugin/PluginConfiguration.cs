﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mt.braecker.fwn.reportplugin
{
    public class PluginConfiguration
    {
        public int ExecutionInterval { get; set; }
        public string WorkFolderPath { get; set; }
        public string DoneFolderPath { get; set; }
        
        public string ImportFolderPath { get; set; }
        public string ImportFileName { get; set; }
        public string PrinterName { get; set; }
        public float XCoordinate { get; set; }
        public float YCoordinate { get; set; }

        public string PdfFile { get; set; } = "export.pdf";

    }
}
