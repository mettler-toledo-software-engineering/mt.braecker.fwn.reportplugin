﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mt.braecker.fwn.reportplugin
{
    public class CsvContent
    {
        public List<CsvLine> CsvLines { get; set; } = new List<CsvLine>();
    }
}
