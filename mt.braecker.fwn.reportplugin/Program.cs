using System.Runtime.CompilerServices;
using mt.braecker.fwn.reportplugin;
using Serilog;
using ILogger = Serilog.ILogger;

//https://docs.microsoft.com/en-us/dotnet/core/extensions/windows-service

IHost host = Host.CreateDefaultBuilder(args)
    .ConfigureAppConfiguration(c =>
    {
        c.AddJsonFile("appsettings.json");
        c.AddEnvironmentVariables();
    })
    .ConfigureServices((context,services) =>
    {
        services.AddSingleton<PluginConfiguration>(CreateConfiguration(context));
        services.AddSingleton<CreatePdfService>();
        services.AddSingleton<CheckForCsvService>();
        services.AddSingleton<FileCleanUpService>();
        services.AddSingleton<PdfPrinterService>();
        services.AddSingleton<FolderCreationService>();
        services.AddSingleton<CsvParser>();
        services.AddHostedService<Worker>();
    })
    .UseWindowsService(options =>
    {
        options.ServiceName = "FreeWeigh Report Plugin";
    })
    .UseSerilog((context, loggerConfiguration) =>
    {
        string logfile = context.Configuration.GetValue<string>("LOGFILE");
        loggerConfiguration
            .WriteTo.File(logfile, rollingInterval: RollingInterval.Day)
            .WriteTo.Debug()
            .MinimumLevel.Information();
    })
    .Build();


ILogger logger = host.Services.GetRequiredService<ILogger>();

PluginConfiguration CreateConfiguration(HostBuilderContext context)
{
    PluginConfiguration configuration = new PluginConfiguration();

    configuration.ExecutionInterval = context.Configuration.GetValue<int>("Import_Folder_Check_Interval_IN_S");
    configuration.ImportFolderPath = context.Configuration.GetValue<string>("ImportFolderPath");
    configuration.WorkFolderPath = context.Configuration.GetValue<string>("WorkFolderPath");
    configuration.DoneFolderPath = context.Configuration.GetValue<string>("DoneFolderPath");
    configuration.ImportFileName = context.Configuration.GetValue<string>("ImportFileName");
    configuration.PrinterName = context.Configuration.GetValue<string>("PrinterName");
    configuration.XCoordinate = context.Configuration.GetValue<float>("X_Coordinate_MM");
    configuration.YCoordinate = context.Configuration.GetValue<float>("Y_Coordinate_MM");
    

    return configuration;
}

try
{
    await host.RunAsync();

}
catch (Exception e)
{
    logger.Fatal(e, "The Service could not be started");
}



