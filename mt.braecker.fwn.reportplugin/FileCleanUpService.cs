﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mt.braecker.fwn.reportplugin
{
    public class FileCleanUpService
    {
        private readonly string _pdfFile;
        private readonly string _csvFile;
        private readonly string _sourcePath;
        private readonly string _destinationPath;

        public FileCleanUpService(PluginConfiguration configuration)
        {
            _pdfFile = configuration.PdfFile;
            _csvFile = configuration.ImportFileName;
            _sourcePath = configuration.WorkFolderPath;
            _destinationPath = configuration.DoneFolderPath;
        }

        public bool CleanUpFiles()
        {
            string sourcePdf = Path.Combine(_sourcePath, _pdfFile);
            string sourceCsv = Path.Combine(_sourcePath, _csvFile);            
            string destPdf = Path.Combine(_destinationPath, _pdfFile);
            string destCsv = Path.Combine(_destinationPath, _csvFile);

            if (File.Exists(sourcePdf) && File.Exists(sourceCsv))
            {
                File.Move(sourcePdf,destPdf, true);
                File.Move(sourceCsv, destCsv, true);
                return true;
            }

            return false;
        }
    }
}
