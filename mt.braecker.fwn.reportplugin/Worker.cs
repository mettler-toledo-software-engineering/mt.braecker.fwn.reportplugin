using Microsoft.Extensions.Configuration;

namespace mt.braecker.fwn.reportplugin
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly CreatePdfService _createPdf;
        private readonly CsvParser _csvParser;
        private readonly CheckForCsvService _csvFinder;
        private readonly PdfPrinterService _pdfPrinter;
        private readonly FileCleanUpService _cleanUpService;
        private readonly FolderCreationService _createFolderService;
        private readonly int _delay;
        private bool _workInProgress = false;

        public Worker(ILogger<Worker> logger, PluginConfiguration configuration, CreatePdfService createPdf, CsvParser csvParser, CheckForCsvService csvFinder, PdfPrinterService pdfPrinter, FileCleanUpService cleanUpService, FolderCreationService createFolderService)
        {
            _logger = logger;
            _createPdf = createPdf;
            _csvParser = csvParser;
            _csvFinder = csvFinder;
            _pdfPrinter = pdfPrinter;
            _cleanUpService = cleanUpService;
            _createFolderService = createFolderService;
            _delay = configuration.ExecutionInterval*1000;
        }


        public override Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("starting up the service");
            _createFolderService.CreateNecessaryFolders();

            return base.StartAsync(cancellationToken);
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("the service has been stopped");
            return base.StopAsync(cancellationToken);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {

                try
                {
                    ProcessReports();
                }
                catch (Exception e)
                {
                    _logger.LogError("processing report failed",e);
                    _workInProgress = false;
                }
                

                await Task.Delay(_delay, stoppingToken);
            }
        }

        private void ProcessReports()
        {
            bool filefound = _csvFinder.FileFound();
            if (filefound && _workInProgress == false)
            {
                _logger.LogInformation("Report Found");
                _workInProgress = true;
                var content = _csvParser.ReadCsv();
                _logger.LogInformation("csv parsed");
                _createPdf.CreatePdfDocument(content);
                _logger.LogInformation("pdf created");
                _pdfPrinter.Print();
                _logger.LogInformation("pdf printed");
                bool filesmoved = _cleanUpService.CleanUpFiles();
                if (filesmoved)
                {
                    _workInProgress = false;
                    _logger.LogInformation("process finished {time}", DateTimeOffset.Now);
                }
            }
        }
    }
}