﻿
namespace mt.braecker.fwn.reportplugin
{
    public class CsvParser
    {
        private readonly PluginConfiguration _configuration;

        public CsvParser(PluginConfiguration configuration)
        {
            _configuration = configuration;
        }

        public CsvContent ReadCsv()
        {
            CsvContent content = new CsvContent();

            string sourceFile = Path.Combine(_configuration.WorkFolderPath, _configuration.ImportFileName);

            string[] lines = File.ReadAllLines(sourceFile);
            foreach (string line in lines)
            {
                CsvLine csvLine = new CsvLine();
                string[] columns = line.Split(';');
                foreach (string column in columns)
                {
                    if (column.Equals("\"\"") || string.IsNullOrWhiteSpace(column) || column.Equals("\n"))
                    {
                        continue;
                    }

                    Item item = new Item{ Content = column.Replace("\"", string.Empty) };
                    csvLine.Items.Add(item);
                }
                content.CsvLines.Add(csvLine);
            }



            return content;
        }
    }
}
