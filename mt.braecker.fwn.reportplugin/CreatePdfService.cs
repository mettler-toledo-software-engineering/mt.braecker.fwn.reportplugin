﻿using System.Net.Mime;
using iText.IO.Image;
using iText.Kernel.Colors;
using iText.Kernel.Font;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Action;
using iText.Kernel.Pdf.Canvas.Draw;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using iText.Pdfa;

namespace mt.braecker.fwn.reportplugin
{
    public class CreatePdfService
    {
        private readonly PluginConfiguration _configuration;

        private float _pointValue = float.Parse("2.8346456693");

        public CreatePdfService(PluginConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void CreatePdfDocument(CsvContent csvContent)
            {

            string pdf = _configuration.PdfFile;

            string pdffile = Path.Combine(_configuration.WorkFolderPath, pdf);
  
            ////Fonts need to be embedded
            PdfFont font = PdfFontFactory.CreateFont(@"c:\windows\fonts\arial.ttf", PdfFontFactory.EmbeddingStrategy.PREFER_EMBEDDED);
            Style normalStyle = new Style();
            
            normalStyle.SetFont(font).SetFontSize(8);
            SolidLine line = new SolidLine(0.5f);
            line.SetColor(ColorConstants.BLACK);
            LineSeparator ls = new LineSeparator(line);
            

            
            var pdfDocument = new PdfDocument(new PdfWriter(pdffile));
            
            var pageSize = pdfDocument.GetDefaultPageSize();
            var document = new Document(pdfDocument, pageSize);
 
            var leftmargin = _configuration.XCoordinate * _pointValue;
            var bottommargin = _configuration.YCoordinate * _pointValue;

            

            document.SetMargins(0, 0, 0, 0);
            Div div = new Div();
            float yincrement = 10;
            foreach (var csvline in csvContent.CsvLines)
            {
                float xincrement = 0;
                
                foreach (var item in csvline.Items)
                {
                    Paragraph paragraph = new Paragraph();
                    paragraph.Add(new Text($"{item.Content}\n").AddStyle(normalStyle));
                    paragraph.SetFixedPosition(leftmargin + xincrement, bottommargin + yincrement, pageSize.GetWidth());
                    div.Add(paragraph);
                    xincrement = xincrement + (14 * _pointValue);
                }
                ls.SetFixedPosition(leftmargin , 0, xincrement);
                ls.SetMarginTop(pageSize.GetHeight() - bottommargin - 11);
                div.Add(ls);
                yincrement = 0;
               
            }

            document.Add(div);
            
            document.Close();

        }


    }
}
